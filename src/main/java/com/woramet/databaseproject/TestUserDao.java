/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.woramet.databaseproject;

import com.woramet.databaseproject.dao.UserDao;
import com.woramet.databaseproject.helper.DatabaseHelper;
import com.woramet.databaseproject.model.User;

/**
 *
 * @author User
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();

        System.out.println("---------------getAll---------------");
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }

//        System.out.println("---------------getOne---------------");
//        User user1 = userDao.get(2);
//        System.out.println(user1);

//        System.out.println("---------------save---------------");
//        User newUser = new User("user3", "password", 2, "F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M");

//        System.out.println("---------------update---------------");
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);

//        System.out.println("---------------delete---------------");
//        userDao.delete(user1);
//        for (User u : userDao.getAll()) {
//            System.out.println(u);
//        }

        System.out.println("---------------getAllWhere---------------");
        for (User u : userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc ")) {
            System.out.println(u);
        }
        
        
        DatabaseHelper.close();
    }
}
